#include "generator.ih"

void Generator::polymorphicOpAssignDecl(ostream &out) const
{
    key(out);

    for (auto &poly: d_polymorphic)
        out << 
            setw(8) << "" <<
                 "SType &operator=(" << poly.second << " const &value);\n" <<
            setw(8) << "" <<
                "SType &operator=(" << poly.second << " &&tmp);\n"
            "\n";
}



